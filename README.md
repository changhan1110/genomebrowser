UCSC Genome Browser
==============

### Assembly Hubs
The Assembly Hub function allows you to display your novel genome sequence using the UCSC Genome Browser.

http://genomewiki.ucsc.edu/index.php/Assembly_Hubs

### Genome upload

One has to process GenBank files into UCSC style fasta and AGP files.

.fasta -> .2bit, chrom.sizes

    cd /data/genomes/ricCom1/ucsc
    faToTwoBit ricCom1.ucsc.fa.gz chrCp.fa ../ricCom1.unmasked.2bit
    cd /data/genomes/ricCom1
    twoBitInfo ricCom1.unmasked.2bit stdout | sort -k2nr > chrom.sizes


### 1. Assembly and gap tracks
The assembly and gap tracks are constructed directly from the agp file:

    mkdir /data/genomes/ricCom1/bed/assemblyTrack
    cd /data/genomes/ricCom1/bed/assemblyTrack
    grep -v "^#" ../../ucsc/ricCom1.ucsc.agp | awk '$5 != "N"' | awk '{printf "%s\t%d\t%d\t%s\t0\t%s\n", $1, $2, $3, $6, $9}' | sort -k1,1 -k2,2n > ricCom1.assembly.bed
    grep -v "^#" ../../ucsc/ricCom1.ucsc.agp | awk '$5 == "N"' | awk '{printf "%s\t%d\t%d\t%s\n", $1, $2, $3, $8}' | sort -k1,1 -k2,2n > ricCom1.gap.bed
    bedToBigBed -verbose=0 ricCom1.assembly.bed ../../chrom.sizes ricCom1.assembly.bb
    bedToBigBed -verbose=0 ricCom1.gap.bed ../../chrom.sizes ricCom1.gap.bb

Track hub trackDb.txt entries:

    track assembly_
    longLabel Assembly
    shortLabel Assembly
    priority 10
    visibility pack
    colorByStrand 150,100,30 230,170,40
    color 150,100,30
    altColor 230,170,40
    bigDataUrl bbi/ricCom1.assembly.bb
    type bigBed 6
    html ../trackDescriptions/assembly
    url http://www.ncbi.nlm.nih.gov/nuccore/$$
    urlLabel NCBI Nucleotide database
    group map

    track gap_
    longLabel Gap
    shortLabel Gap
    priority 11
    visibility dense
    color 0,0,0
    bigDataUrl bbi/ricCom1.gap.bb
    type bigBed 4
    group map
    html ../trackDescriptions/gap

### 2. GC Percent
The calculation of the GC Percent track does not require masked sequence.
You can construct this track directly from the unmasked.2bit file:

    mkdir /data/genomes/ricCom1/bed/gc5Base
    cd /data/genomes/ricCom1/bed/gc5Base
    hgGcPercent -wigOut -doGaps -file=stdout -win=5 -verbose=0 ricCom1 \
      ../../ricCom1.unmasked.2bit | gzip -c > ricCom1.gc5Base.wigVarStep.gz
    wigToBigWig ricCom1.gc5Base.wigVarStep.gz ../../chrom.sizes ricCom1.gc5Base.bw
Track hub trackDb.txt file entry:

    track gc5Base_
    shortLabel GC Percent
    longLabel GC Percent in 5-Base Windows
    group map
    priority 23.5
    visibility full
    autoScale Off
    maxHeightPixels 128:36:16
    graphTypeDefault Bar
    gridDefault OFF
    windowingFunction Mean
    color 0,0,0
    altColor 128,128,128
    viewLimits 30:70
    type bigWig 0 100
    bigDataUrl bbi/ricCom1.gc5Base.bw
    html ../trackDescriptions/gc5Base

### 3. Repeat Masking
Repeat masking is necessary for almost all other track construction.

#### Reference
* http://genomewiki.ucsc.edu/index.php/Browser_Track_Construction
* http://genomewiki.ucsc.edu/index.php/Assembly_Hubs
* http://genomewiki.ucsc.edu/index.php/CPG_Islands
* http://genomewiki.ucsc.edu/index.php/Genscan
* http://genomewiki.ucsc.edu/index.php/RepeatMasker
* http://genomewiki.ucsc.edu/index.php/Window_Masker
* http://genomewiki.ucsc.edu/index.php/TRF_Simple_Repeats
* http://oliverelliott.org/article/bioinformatics/tut_genomebrowser/